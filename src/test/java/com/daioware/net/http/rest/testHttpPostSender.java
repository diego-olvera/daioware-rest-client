package com.daioware.net.http.rest;


import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import com.daioware.net.http.HttpResponse;
import com.daioware.net.http.HttpUtil;
import com.daioware.net.http.client.HandlingException;
import com.daioware.net.http.client.ResponseBodyException;
import com.daioware.net.http.items.HttpHeader;
import com.daioware.net.http.items.QueryParameter;
import com.daioware.net.http.items.QueryParameterList;
import com.daioware.net.http.request.HttpGetRequest;
import com.daioware.net.http.request.HttpPostRequest;
import com.daioware.net.http.request.HttpRequest;
import com.daioware.net.http.rest.request.RestPostRequest;

public class testHttpPostSender {

	public static void setHeadersForSendingQueryParams(HttpRequest req) {
		req.addHeader("Content-Type","application/x-www-form-urlencoded;charset=utf-8;");
		req.addHeader("Accept","application/json");
		req.addHeader("Connection","close");
	}
	public static void setHeadersForSendingObjects(HttpRequest req) {
		req.addHeader("Content-Type","application/json;charset=utf-8;");
		req.addHeader("Accept","application/json");
		req.addHeader("Connection","close");
	}
	public static void addSessionHeaders(HttpRequest req,Session session) {
		req.addHeader("sessionId",String.valueOf(session.getId()));
		req.addHeader("sessionValue",session.getValue());
	}
	public static void main(String[] args) throws UnknownHostException, IOException, ResponseBodyException, HandlingException {
		String urlForLogin="http://localhost:8080/storeTracker/session/logIn";
		Long productId=1L;
		String urlForProductPriceHistory="http://localhost:8080/storeTracker/productPriceHistory/"+productId;
		String urlForLogout="http://localhost:8080/storeTracker/session/logOut";
		HttpPostRequest loginRequest=new HttpPostRequest(urlForLogin);
		RestPostRequest<Session> logoutRequest=new RestPostRequest<>(urlForLogout);

		HttpGetRequest productPriceHistoryRequest=new HttpGetRequest(urlForProductPriceHistory);
		RestResponse getProductPriceHistoryResponse;
		RestResponse sessionResponse;
		QueryParameterList parametersList=new QueryParameterList();
		setHeadersForSendingQueryParams(loginRequest);
		parametersList.add("email","diego6569olvera@gmail.com");
		parametersList.add("password","password");

		loginRequest.setBodyStr(parametersList.toString(loginRequest.getEncoding()));
		RestSender sender=new RestSender(loginRequest);
		sender.setDownloadSpeedInBytes(512);
		
		printRequest(loginRequest,"Login");

		RestResponse loginResponse=sender.send(0);
		assertNotNull(loginResponse);
		
		printResponse(loginResponse,"Login",true);
		
		Map<String,Boolean> configs=new HashMap<>();
		configs.put(RestResponse.FAIL_ON_UNKNOWN_PROPERTIES,false);
		Session session=loginResponse.getContent(Session.class,configs);
		assertNotNull(session);
		assertNotNull(session.getValue());
		assertNotNull(session.getId());
		
		addSessionHeaders(productPriceHistoryRequest,session);
		
		setHeadersForSendingQueryParams(productPriceHistoryRequest);
		
		productPriceHistoryRequest.addQueryParam("page",String.valueOf(0));
		productPriceHistoryRequest.addQueryParam("size",String.valueOf(100));
		printRequest(productPriceHistoryRequest,"Get product price history");
		RestSender restSender=new RestSender(productPriceHistoryRequest);
		getProductPriceHistoryResponse=(RestResponse) restSender.send(0);
		
		printResponse(getProductPriceHistoryResponse,"GetProductPriceHistory",true);
		
		setHeadersForSendingObjects(logoutRequest);
		addSessionHeaders(logoutRequest,session);

		logoutRequest.setContent(session);
		RestSender logOutSender=new RestSender(logoutRequest);

		logOutSender.setRequest(logoutRequest);
		printRequest(logoutRequest, "Logout");
		sessionResponse=(RestResponse) logOutSender.send(0);
		printResponse(sessionResponse, "Log out", true);
		Boolean isSessionLoggedOut=sessionResponse.getContent(Boolean.class);
		
		assertNotNull(isSessionLoggedOut);
		assertTrue(isSessionLoggedOut);
	}
	
	public static void printRequest(HttpRequest request,String requestName) throws UnsupportedEncodingException {
		System.out.println(requestName+" Request ");
		System.out.println("\t"+request.getUrl());
		System.out.println("\tHEADERS");
		for(HttpHeader header:request.getHeaders()) {
			System.out.println("\t\t"+header.toString(HttpUtil.DEFAULT_ENCODING));
		}
		System.out.println("\tPARAMS");
		for(QueryParameter queryParameter:request.getQueryParameters()) {
			System.out.println("\t\t"+queryParameter.toString(HttpUtil.DEFAULT_ENCODING));
		}
	}
	
	public static void printResponse(HttpResponse response,String responseName,boolean printAsString) throws UnsupportedEncodingException, ResponseBodyException {
		System.out.println(responseName+ " Response");
		System.out.println("\tStatus:"+response.getStatus()+", message:"+response.getStatusMessage());
		System.out.println("\tHeaders");
		for(HttpHeader header:response.getHeaders().values()) {
			System.out.println("\t\t"+header.toString(HttpUtil.DEFAULT_ENCODING));
		}
		if(printAsString) {
			System.out.println("\tBody");
			System.out.println("\t\t"+response.getBodyAsString());
		}
	}
}
