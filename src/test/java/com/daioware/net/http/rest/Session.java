package com.daioware.net.http.rest;


import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Session {
	
	public static final char MOBILE='M';
	public static final char DESKTOP='D';
	public static final char WEB_SERVICES='W';
	public static final char WEB_PAGE='P';
	public static final char UNDEFINED='U';
	
	private Long id;
	
	@JsonIgnore
	private LocalDateTime insertionDate;
	@JsonIgnore
	private LocalDateTime lastUpdateDate;
	@JsonIgnore
	private LocalDateTime expirationDate;

	private User user;
	private String ipAddress;
	private String value;
	private boolean enabled=true;
	private char platform;
	
	public Session() {
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getInsertionDate() {
		return insertionDate;
	}

	public void setInsertionDate(LocalDateTime insertionDate) {
		this.insertionDate = insertionDate;
	}
	
	public LocalDateTime getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(LocalDateTime lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public LocalDateTime getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(LocalDateTime expirationDate) {
		this.expirationDate = expirationDate;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public char getPlatform() {
		return platform;
	}

	public void setPlatform(char platform) {
		this.platform = platform;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	void onCreate() {
		LocalDateTime date=LocalDateTime.now();
		setInsertionDate(date);
		setLastUpdateDate(date);
	}

	void onPersist(){
		setLastUpdateDate(LocalDateTime.now());
	}
	@Override
	public String toString() {
		return "Session [id=" + id + ", insertionDate=" + insertionDate + ", updateDate=" + lastUpdateDate + ", ipAddress="
				+ ipAddress + ", value=" + value + ", enabled=" + enabled + ", user= "+user+"]";
	}

	
}

