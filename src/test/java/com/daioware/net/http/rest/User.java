package com.daioware.net.http.rest;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;


public class User{

	public static final char ADMIN_USER='A';
	public static final char REGULAR_USER='R';
	
	private Long id;
	private String firstName;
	private String middleName;
	private String lastName;
	private String email;
	private String password;
	private String fullName;
	private char status;
	
	private int maxSimultaneousConnections;
	private char accountType;

	@JsonIgnore
	private Set<Object> productsTracking;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		
	}

	public String getFirstName() {
		return firstName;
	}
	

	public String getMiddleName() {
		return middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getEmail() {
		return email;
	}
	
	public String getFullName() {
		return fullName;
	}
	
	public String getPassword() {
		return password;
	}
	
	public Set<Object> getProductsTracking() {
		return productsTracking;
	}

	public char getStatus() {
		return status;
	}

	
	public void setStatus(char status) {
		this.status = status;
	}

	public void setProductsTracking(Set<Object> productTracking) {
		this.productsTracking = productTracking;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setEmail(String e){
		email=e;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "User [email="
				+ email + ", status=" + status
				+ ", productsTracking=" + productsTracking + "]";
	}


	public int getMaxSimultaneousConnections() {
		return maxSimultaneousConnections;
	}

	public char getAccountType() {
		return accountType;
	}

	public void setMaxSimultaneousConnections(int maxSimultaneousConnections) {
		this.maxSimultaneousConnections = maxSimultaneousConnections;
	}

	public void setAccountType(char accountType) {
		this.accountType = accountType;
	}

}
