package com.daioware.net.http.rest.request;

import java.net.MalformedURLException;
import java.net.URL;

import com.daioware.net.http.request.HttpPostRequest;

public class RestPutRequest<ItemType> extends HttpPostRequest{

	public RestPutRequest(String url, String encoding) throws MalformedURLException {
		super(url, encoding);
	}

	public RestPutRequest(String url) throws MalformedURLException {
		super(url);
	}

	public RestPutRequest(URL url, String encoding) {
		super(url, encoding);
	}

	public RestPutRequest(URL url) throws MalformedURLException {
		super(url);
	}

	@Override
	public String getMethod() {
		return "PUT";
	}
	
	
}
