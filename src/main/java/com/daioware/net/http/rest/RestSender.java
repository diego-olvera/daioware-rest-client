package com.daioware.net.http.rest;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

import com.daioware.net.http.client.HandlingException;
import com.daioware.net.http.client.HttpSender;
import com.daioware.net.http.items.HttpHeader;
import com.daioware.net.http.request.HttpRequest;
import com.daioware.net.socket.TCPClientSocket;

public class RestSender extends HttpSender{

	public RestSender(HttpRequest request, int downloadSpeedInBytes, int uploadSpeedInBytes) {
		super(request, downloadSpeedInBytes, uploadSpeedInBytes);
	}

	public RestSender(HttpRequest request) {
		super(request);
	}

	@Override
	protected RestResponse createHttpResponse(Map<String, HttpHeader> headers, List<Byte> bodyBytes, String version,
			Integer status, String statusMessage, HttpRequest request){
		return new RestResponse(headers, bodyBytes, version, status, statusMessage,request);
	}

	@Override
	public RestResponse send() throws UnknownHostException, IOException, HandlingException {
		return (RestResponse) super.send();
	}

	@Override
	public RestResponse send(int timeOutMillis) throws UnknownHostException, IOException, HandlingException {
		return (RestResponse) super.send(timeOutMillis);
	}

	@Override
	public RestResponse send(TCPClientSocket socket)
			throws UnsupportedEncodingException, IOException, HandlingException {
		return (RestResponse) super.send(socket);
	}

	
}
