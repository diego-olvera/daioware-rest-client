package com.daioware.net.http.rest;

import com.fasterxml.jackson.databind.DeserializationFeature;

public class JsonConfig {

	private DeserializationFeature feature;
	private Boolean value;
	
	
	public JsonConfig(DeserializationFeature feature, Boolean value) {
		setFeature(feature);
		setValue(value);
	}
	public DeserializationFeature getFeature() {
		return feature;
	}
	public Boolean getValue() {
		return value;
	}
	public void setFeature(DeserializationFeature feature) {
		this.feature = feature;
	}
	public void setValue(Boolean value) {
		this.value = value;
	}
	
	
}
