package com.daioware.net.http.rest;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.daioware.commons.wrapper.WrapperString;
import com.daioware.net.http.HttpResponse;
import com.daioware.net.http.HttpUtil;
import com.daioware.net.http.client.ResponseBodyException;
import com.daioware.net.http.items.HttpHeader;
import com.daioware.net.http.request.HttpRequest;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

public class RestResponse extends HttpResponse{
	
	public static final String FAIL_ON_UNKNOWN_PROPERTIES="FAIL_ON_UNKNOWN_PROPERTIES";
	
	private static final Map<String,DeserializationFeature> desearializationFeatures=new HashMap<>();
	
	protected HttpHeader defaultContentTypeHeader;
	
	private Object response;
	
	static {
		desearializationFeatures.put(FAIL_ON_UNKNOWN_PROPERTIES, DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
	}
	
	public RestResponse(Map<String, HttpHeader> headers, List<Byte> body, String version, Integer status,
			String statusMessage, HttpRequest request) {
		super(headers, body, version, status, statusMessage, request);
	}

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		this.response = response;
	}
	@SuppressWarnings("unchecked")
	public <ReturnType> ReturnType getContent(Class<ReturnType> clazz,Map<String,Boolean> configurations) throws ResponseBodyException {
		if(getResponse()==null) {
			setResponse(convertRestResponseToReturnType(clazz,configurations));
		}
		return (ReturnType)getResponse();
	}
	
	public <ReturnType> ReturnType getContent(Class<ReturnType> clazz) throws ResponseBodyException {
		return getContent(clazz,new HashMap<>());
	}
	
	protected <ReturnType> ReturnType convertRestResponseToReturnType(Class<ReturnType> clazz) throws ResponseBodyException{
		return convertRestResponseToReturnType(clazz, new HashMap<>(0));
	}
	protected <ReturnType> ReturnType convertRestResponseToReturnType(Class<ReturnType> clazz,
			Map<String,Boolean> configurations) throws ResponseBodyException {
		HttpHeader header=getHeaders().get("Content-Type");
		ReturnType returnElement;
		WrapperString mime=new WrapperString();
		WrapperString charset=new WrapperString();
		ObjectMapper jsonMapper; 
		XmlMapper xmlMapper;
		if(header!=null) {
			HttpUtil.parseContentType(mime, charset, header.getValue());
			switch(mime.value) {
				case "application/json":
					try {
						jsonMapper=new ObjectMapper();
						configureObjectMapper(jsonMapper,configurations);
						returnElement=jsonMapper.readValue(getBodyAsString(Charset.forName(charset.value)),clazz);
					}
					catch (IOException e) {
						throw new ResponseBodyException(e);
					}
					break;
				case "application/xml":
					xmlMapper = new XmlMapper();
					try {
						configureObjectMapper(xmlMapper,configurations);
						returnElement=xmlMapper.readValue(getBodyAsString(Charset.forName(charset.value)), clazz);
					} catch (IOException e) {
						throw new ResponseBodyException(e);
					}
					break;
				default:throw new ResponseBodyException("Content-Type '"+mime.value+"' not valid");
			}
		}
		else {
			throw new ResponseBodyException("No Content-Type header in response");
		}
		return returnElement;
	}

	protected static void configureObjectMapper(ObjectMapper jsonMapper, Map<String,Boolean> configurations) {
		for(String key:configurations.keySet()){
			jsonMapper.configure(desearializationFeatures.get(key), configurations.get(key));
		}
	}

	
	
}
