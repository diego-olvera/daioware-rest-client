package com.daioware.net.http.rest.request;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

import com.daioware.commons.wrapper.WrapperString;
import com.daioware.net.http.HttpUtil;
import com.daioware.net.http.items.HttpHeader;
import com.daioware.net.http.request.HttpPostRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

public class RestPostRequest<ItemType> extends HttpPostRequest{

	public RestPostRequest(String url, String encoding) throws MalformedURLException {
		super(url, encoding);
	}

	public RestPostRequest(String url) throws MalformedURLException {
		super(url);
	}

	public RestPostRequest(URL url, String encoding) {
		super(url, encoding);
	}

	public RestPostRequest(URL url) throws MalformedURLException {
		super(url);
	}
	
	public void setContent(ItemType t) throws IOException{
		ObjectMapper jsonMapper; 
		XmlMapper xmlMapper;
		WrapperString mimeType;
		WrapperString charset;
		for(HttpHeader header:getHeaders()) {
			if("Content-Type".equals(header.getKey())) {
				mimeType=new WrapperString();
				charset=new WrapperString();
				HttpUtil.parseContentType(mimeType, charset, header.getValue());
				switch(mimeType.value) {
					case "application/json":
						jsonMapper=new ObjectMapper();
						setBodyStr(new String(jsonMapper.writeValueAsString(t).getBytes(Charset.forName(charset.value)),
								charset.value));
						return;
					case "application/xml":
						xmlMapper = new XmlMapper();
						setBodyStr(new String(xmlMapper.writeValueAsString(t).getBytes(Charset.forName(charset.value)),
								charset.value));
						return;
					default: throw new IOException("Content-Type '"+mimeType.value+"' not valid");
				}
			}
		}
		throw new IOException("There is no Content-Type header in the request");
	}

}
